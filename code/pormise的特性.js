var fs = require('fs');

/* var promise = new Promise(function () {
    fs.readFile('./files/3.txt', 'utf-8', (err, dataStr) => {
        if (err) {
            throw err//抛出
        }
        console.log(dataStr);

    })
})
 */

 //第一步
function getFileBypath(fpath) {
    //第三步 到这代码还是异步的 没有执行完
    var promise = new Promise(function (resolve,reject) {
        // 读取完开始执行resolve,reject
        fs.readFile(fpath,'utf-8',(err,dataStr)=>{
            if(err){
                reject(err) 
            }
            resolve(dataStr);
        })
    })

    // 第四步
    return promise
}

// 第二步  同时调用.then（）方法
// 第五步 返回实例
var p = getFileBypath('./files/1.txt')
// 第六步 
p.then(function (data) {
    console.log(data);
    
},function (err) {
    console.log(err.message);
    
})

